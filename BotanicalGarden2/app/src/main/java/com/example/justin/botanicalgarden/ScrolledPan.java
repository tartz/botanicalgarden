package com.example.justin.botanicalgarden;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ScrolledPan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolled_pan);

        Button enter = (Button) findViewById(R.id.toPlant);
        enter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ScrolledPan.this, PlantView.class);
                startActivity(intent);


                /*Toast toast = Toast.makeText(getApplicationContext(), "Clicked button", Toast.LENGTH_SHORT);
                toast.show();*/
            }

        });

        enter = (Button) findViewById(R.id.mapReturn);
        enter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ScrolledPan.this, MapView.class);
                startActivity(intent);


                /*Toast toast = Toast.makeText(getApplicationContext(), "Clicked button", Toast.LENGTH_SHORT);
                toast.show();*/
            }

        });
    }
}
