package com.example.justin.botanicalgarden;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class PlantView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_view);

        /*ImageView plantImage = (ImageView)findViewById(R.id.imageView4);
        plantImage.setImageResource(R.drawable.uncc_cone_entry1);*/


        Button enter = (Button) findViewById(R.id.button);
        enter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PlantView.this, ScrolledPan.class);
                startActivity(intent);


                /*Toast toast = Toast.makeText(getApplicationContext(), "Clicked button", Toast.LENGTH_SHORT);
                toast.show();*/
            }

        });
    }
}
